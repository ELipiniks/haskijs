//Šis skripts nodrošina to, ka formās veiksmīgi nosūtot datus ar POST/GET, atsvaidzinot
//lapu nenotiek atkārtota sūtīšana un lieku ierakstu izveidošana
//alternatīva metode šim visam ir Post/Redirect/Get (PRG), bet šo vēl jāpēta
//tagadējais risinājums ir tikai pagaidu.
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
