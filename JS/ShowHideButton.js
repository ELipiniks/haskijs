// Jquerry+JS skripts, kurš parāda dzēšanas pogu, piespiežot jebkuru checkbox

$(document).ready(function() {

    var $submit = $("#submit_prog").hide(),
        $cbs = $('input[name="checkbox[]"]').click(function() {
            $submit.toggle( $cbs.is(":checked") );
        });

});
