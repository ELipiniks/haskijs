<?php
session_start();
if(!isset($_SESSION['loggedin'])){
	header('Location: index.php');
	exit;
}
?>
<!-- Visi pievienotie PHP kodi -->
<?php include './Functionality/ConnectToDB.php'; ?>
<?php include './Functionality/PrepareStatement.php'; ?>
<?php include './Functionality/EditRecord.php'; ?>
<?php include './Functionality/collectData.php'; ?>




<!DOCTYPE html>
<!-- Valodas direktīva -->
<html lang="lv" dir="ltr">

<head>
  <!--Responsivitātes parametrs-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!-- PreventResubmit, lai izvairītos no atkārtotas dublikātu nosūtīšanas uz datubāzi -->
  <script src="./JS/PreventResubmit.js"></script>
  <script src="./JS/jquery-3.5.1.min.js"></script>
  <!-- Meklēšanas ailes skripts, lai tabulā meklētu ierakstus -->
  <script src="./JS/SearchRecord.js"></script>
  <script src="./JS/Snackbar.js"></script>
  <script src="./JS/ShowHideButton.js"></script>
  <!-- <script src="./JS/ShowHideButton.js"></script> -->
  <!--Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  <!--CSS ceļš -->
  <link rel="stylesheet" href="/Style/style.css">
  <!--Lapas nosaukums-->
  <title>Husky</title>

</head>

<body>
  <!--Apvalks visais lapai, papildus darbina grid -->
  <div class="wrapper">
    <!-- Lapas nosaukums un pārējā būtiskā informācija -->
    <header>
      <!-- Lapas nosaukums, galvenais Headeris -->
      <h1>ADVERTISEMENT</h1>
      <!--Kas autorizējies un iespēja izlogoties.  -->
      <div class="Header_Login">
        <a href="/profile.php?user=<?=$_SESSION['username']?>"><?= $_SESSION['username'] ?></a>
        <br/>
        <a href="/Functionality/logout.php">IZLOGOTIES</a>
      </div>
    </header>
    <!--Navigācijas sadaļa, lai "Staigātu" starp lapām -->
    <nav>
      <!--Lapas nosaukuma konteineris -->
      <div class="PageTitle">
        <h1>H U S K Y</h1>
        <!-- Līnija, kas sadala h1 un h3 -->
        <div class="DividerLine"></div>
        <h3>SATURA ADAPTĀCIJAS SASKARNE</h3>
      </div>
      <!-- URL'S -->
			<a href="main.php">SĀKUMS</a>
			<a href="super_blacklist.php">SUPER-BLACKLIST</a>
			<a href="blacklist.php">BLACKLIST</a>
			<a href="whitelist.php">WHITELIST</a>
			<a href="super_whitelist.php">SUPER-WHITELIST</a>
			<a href="ssl_intercept.php">SSL INTERCEPT</a>
			<a href="without_ssl_intercept.php">WITHOUT SSL INTERCEPT</a>
			<a href="with_authentication.php">WITH AUTHENTICATION</a>
			<a href="without_authentication.php">WITHOUT AUTHENTICATION</a>
			<a href="do_not_scan.php">DO NOT SCAN</a>
			<a href="advertisement.php" style="color:#8994b6;">ADVERTISEMENT</a>
    </nav>
    <!-- Satura ievades un pārskata sadaļa -->
    <main>
      <div class="InputForm">
        <form method="post" name="myform" action="<?php $_PHP_SELF ?>">
          <!-- Pievienojamais URL/Domēns - Aile ir obligāta -->
          <textarea name="Address_Value" placeholder="PIEVIENOJAMAIS URL/DOMĒNS" required></textarea>
          <!-- Pievienošanas iemesla aile - Aile ir obligāta  -->
          <textarea name="Submit_Reason" placeholder="PIEVIENOŠANAS IEMESLS" required></textarea>
          <button type="submit" name="submit">PIEVIENOT</button>
        </form>
        <!-- PHP skripts, kurš pēc formas submit nospiešanas nosūta visus datus uz SQL datubāzi -->
        <?php addFormRecord("Advertisement", $_SESSION['username']) ?>

      </div>

      <form class="Record_List" action="delete.php?category=Advertisement" method="post">
        <!-- Pogas, lai kontrolētu datu pievienošanu un dzēšanu -->
        <div class="SearchAndSave">
          <input type="text" id="myInput" onkeyup="SearchRecord()" placeholder="Meklēt.." title="Type in a name">

          <button><a href='/Functionality/SaveToFile.php?category=Advertisement&page=sub?user=<?= $_SESSION['username'] ?>'>SAGLABĀT FAILĀ</a></button>
          <button><a href='/upload.php?category=Advertisement'>AUGŠUPIELĀDĒT FAILU</a></button>
          <button type="submit" id="submit_prog">DZĒST</button>
        </div>
  <!-- Tabula attēlot ierakstus -->
        <table style="width:100%" id="myTable">
          <tr>
            <th width="4%">ID</th>
            <th width="30%">ADRESE</th>
            <th width="20%">PIEVIENOŠANAS IEMESLS</th>
            <th width="7%">VEIDS</th>
            <th width="7%">METODE</th>
            <th width="5%">LABOTS</th>
            <th width="15%">DATUMS | LAIKS</th>
            <th width="10%">AUTORS</th>
            <th width="5%"></th>
            <th width="5%"></th>
          </tr>
          <!-- PHP skripts, kurš iegūst visus ierakstus no SQL datubāzes -->
          <?php retrieveRecords("Advertisement") ?>
        </table>
      </form>

    </main>
</body>

</html>
