# Haskijs

/***************************
* Pieslēgšanās pie datubāzes
****************************/

Fails: /Functionality/config.ini
Fails satur:

[database]
username = root //Datubāzes lietotājvārds
password = password //Datubāzes parole
dbname = myDB //Datubāzes nosaukums
localAdmin = LocalAdmin //Lokālais lietotājs un viņa lietotājvārds
localPassword = password //Lokālā lietotāja parole

username un password var mainīt pēc saviem ieskatiem un jāliek tie paši autentifikācijas dati, kas tiek lietoti iekš mariaDB.
!!!UZMANĪBU!!!: Datubāze sākumā var nedarboties. Labojums: Manuāli izveidot datubāzi

/*********************************
* Pieslēgšanās ar lokālo lietotāju
**********************************/

Tajā pašā config ini ir localAdmin un localPassword, šo var mainīt pēc saviem ieskatiem.
Sistēma tālāk pati šo visu apstrādās un iedos tiesības.

/******************************
*Jaunu kategoriju pievienošana
*******************************/
Galvenajā html lapā izveidot jaunu .php failu, vēlams kādas kategorijas kopiju un nosaukt to failu jaunās kategorijas vārdā
Atverot kategoriju labošanai jālabo sekojošas rindas:
48 rinda: h1 nosaukums
66 - 76. rinda, pievienojam jaunās kategorijas saiti un to atjaunināt pārējās kategorijās
89. rinda: addFormRecord kategorijas nosaukums (Case sensitive - iegaumē šo nosaukumu)
93. Rinda delete.php?category = tas pats nosaukums, ko lietojat iekš 89. rindas
98. Rinda SaveToFile.php?category = tas pats nosaukums
99. Rinda, arī aiz category= to pašu nosaukumu
117. Rinda retrieveRecords , ielikt to pašu nosaukumu

Doties uz main.php

Izveidot vēl vienu h3 un iekš countRecords iekavās ielikt to pašu nosaukumu

Doties uz /Functionality/CountRecords.php
Atkomentēt pašās beigās šablonu un ielikt iekš XXX savu kategoriju : SELECT COUNT(ID) AS SKAITS FROM XXXXX WHERE  

Doties uz Functionality/saveAllFiles.php :
10. rinda: Iekš array, ar komatu ielikt jaunu kategoriju
17.Rindā : inkrementēt for ciklā $i<X par vienu.
