<!-- Visi pievienotie PHP kodi -->
<?php
session_start();
if(!isset($_SESSION['loggedin'])){
        header('Location: index.php');
        exit;
}
?>

<!DOCTYPE html>
<!-- Valodas direktīva -->
<html lang="lv" dir="ltr">

<head>
  <!--Responsivitātes parametrs-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!-- PreventResubmit, lai izvairītos no atkārtotas dublikātu nosūtīšanas uz datubāzi -->
  <script src="./JS/PreventResubmit.js"></script>
  <!-- Meklēšanas ailes skripts, lai tabulā meklētu ierakstus -->
  <script src="./JS/SearchRecord.js"></script>
  <!--Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  <!--CSS ceļš -->
  <link rel="stylesheet" href="/Style/style.css">
  <!--Lapas nosaukums-->
  <title>Husky</title>

</head>

<body>
  <!--Apvalks visais lapai, papildus darbina grid -->
  <div class="wrapper">
    <!-- Lapas nosaukums un pārējā būtiskā informācija -->
    <header>
      <!-- Lapas nosaukums, galvenais Headeris -->
      <h1>DZĒŠANAS SADAĻA</h1>
      <!--Kas autorizējies un iespēja izlogoties.  -->
      <div class="Header_Login">
        <a href="/profile.php">
          <?= $_SESSION['username'] ?>
        </a>
        <br />
        <a href="/Functionality/logout.php">IZLOGOTIES</a>
      </div>
    </header>
    <nav>
      <!--Lapas nosaukuma konteineris -->
      <div class="PageTitle">
        <h1>H U S K Y</h1>
        <!-- Līnija, kas sadala h1 un h3 -->
        <div class="DividerLine"></div>
        <h3>SATURA ADAPTĀCIJAS SASKARNE</h3>
      </div>
      <!-- URL'S -->
      <a href="index.php">SĀKUMS</a>
      <a href="super_blacklist.php">SUPER-BLACKLIST</a>
      <a href="blacklist.php">BLACKLIST</a>
      <a href="whitelist.php" style="color:#8994b6;">WHITELIST</a>
      <a href="super_whitelist.php">SUPER-WHITELIST</a>
      <a href="ssl_intercept.php">SSL INTERCEPT</a>
			<a href="without_ssl_intercept.php">WITHOUT SSL INTERCEPT</a>
			<a href="with_authentication.php">WITH AUTHENTICATION</a>
			<a href="without_authentication.php">WITHOUT AUTHENTICATION</a>
			<a href="do_not_scan.php">DO NOT SCAN</a>
			<a href="advertisement.php">ADVERTISEMENT</a>
    </nav>
    <main>
      <!-- Domēnu reģistrēšanas forma -->

      <div class="UploadForm">
	<form method="post" action="/Functionality/delete.php?user=<?=$_SESSION['username']?>">
          <h2>Dzēst izvēlētos ierakstus:</h2>
          <textarea name="Delete_Reason" placeholder="Dzēšanas iemesls" required></textarea>
          <input name="Delete_Data" value="<?php echo htmlspecialchars(implode(",", $_POST['checkbox']));?>" readonly></input>
          <input name="Category" value="<?php echo htmlspecialchars($_GET['category']);?>" readonly></input>
        </br>
          <button type="submit" name="submit">DZĒST</button>
        </form>
      </div>


    </main>
</body>

</html>
