<!DOCTYPE html>
<html lang="lv" dir="ltr">

<head>
  <!--Responsivitātes parametrs-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!--Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  <!--CSS ceļš -->
  <link rel="stylesheet" href="/Style/style.css">
  <script src="./JS/PreventResubmit.js"></script>
  <!--Lapas nosaukums-->
  <title>Husky</title>

</head>

<body>
  <!--Apvalks visais lapai, papildus darbina grid -->
  <div class="wrapper">
    <!-- Autentifikācijas konteineris -->
    <div class="Authentication">

      <div class="Authentication-Form">
        <!-- Aizpildot formu tiek izsaukts Autentifikācijas skripts -->
        <form action="./Functionality/Authentication.php" method="post">
          <div class="PageTitle">
            <h1>H U S K Y</h1>
            <!-- Līnija, kas sadala h1 un h3 -->
            <div class="DividerLine"></div>
            <h3>SATURA ADAPTĀCIJAS SASKARNE</h3>
          </div>
          <h3>AUTENTIFIKĀCIJA</h3>
          <input type="text" name="username" placeholder="LIETOTĀJVĀRDS" id="username" required><br>
          <input type="password" name="password" placeholder="PAROLE" id="password" required><br>
          <input type="submit" value="Ienākt">
        </form>
      </div>
    </div>
  </div>
</body>

</html>
