<?php
session_start();
if(!isset($_SESSION['loggedin'])){
	header('Location: index.php');
	exit;
}
?>
<?php include './Functionality/ConnectToDB.php'; ?>
<?php include './Functionality/PrepareStatement.php'; ?>
<?php include './Functionality/EditRecord.php'; ?>
<?php include './Functionality/CountRecords.php'; ?>


<!DOCTYPE html>
<!-- Valodas direktīva -->
<html lang="lv" dir="ltr">

<head>
  <!--Responsivitātes parametrs-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!-- PreventResubmit, lai izvairītos no atkārtotas dublikātu nosūtīšanas uz datubāzi -->
  <script src="./JS/PreventResubmit.js"></script>
  <!-- Meklēšanas ailes skripts, lai tabulā meklētu ierakstus -->
  <script src="./JS/SearchRecord.js"></script>
  <!--Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  <!--CSS ceļš -->
  <link rel="stylesheet" href="/Style/style.css">
  <!--Lapas nosaukums-->
  <title>Husky</title>

</head>

<body>
  <!--Apvalks visais lapai, papildus darbina grid -->
  <div class="wrapper">
    <!-- Lapas nosaukums un pārējā būtiskā informācija -->
    <header>
      <!-- Lapas nosaukums, galvenais Headeris -->
      <h1>SĀKUMS - INFORMĀCIJAS PĀRSKATS</h1>
      <!--Kas autorizējies un iespēja izlogoties.  -->
      <div class="Header_Login">
	<a href="/profile.php?user=<?=$_SESSION['username']?>"><?= $_SESSION['username'] ?></a>
	<br/>
        <a href="/Functionality/logout.php">IZLOGOTIES</a>
      </div>
    </header>
    <nav>
      <!--Lapas nosaukuma konteineris -->
      <div class="PageTitle">
        <h1>H U S K Y</h1>
        <!-- Līnija, kas sadala h1 un h3 -->
        <div class="DividerLine"></div>
        <h3>SATURA ADAPTĀCIJAS SASKARNE</h3>
      </div>
      <!-- URL'S -->
      <a href="main.php" style="color:#8994b6;">SĀKUMS</a>
      <a href="super_blacklist.php">SUPER-BLACKLIST</a>
      <a href="blacklist.php">BLACKLIST</a>
      <a href="whitelist.php">WHITELIST</a>
      <a href="super_whitelist.php">SUPER-WHITELIST</a>
			<a href="ssl_intercept.php">SSL INTERCEPT</a>
			<a href="without_ssl_intercept.php">WITHOUT SSL INTERCEPT</a>
			<a href="with_authentication.php">WITH AUTHENTICATION</a>
			<a href="without_authentication.php">WITHOUT AUTHENTICATION</a>
			<a href="do_not_scan.php">DO NOT SCAN</a>
			<a href="advertisement.php">ADVERTISEMENT</a>
    </nav>
    <main>
      <!-- Kategoriju ierakstu skaits -->
      <div class="Information">
        <h3>SUPER-BLACKLIST IERAKSTU SKAITS: <?php countRecords("super_blacklist") ?> <button><a href='/Functionality/SaveToFile.php?category=super_blacklist&page=main'>SAGLABĀT FAILĀ</a></button>
          <h3>BLACKLIST IERAKSTU SKAITS: <?php countRecords("blacklist") ?> <button><a href='/Functionality/SaveToFile.php?category=blacklist&page=main'>SAGLABĀT FAILĀ</a></button>
            <h3>WHITELIST IERAKSTU SKAITS: <?php countRecords("whitelist") ?> <button><a href='/Functionality/SaveToFile.php?category=whitelist&page=main'>SAGLABĀT FAILĀ</a></button>
              <h3>SUPER-WHITELIST IERAKSTU SKAITS: <?php countRecords("super_whitelist") ?><button><a href='/Functionality/SaveToFile.php?category=super_whitelist&page=main'>SAGLABĀT FAILĀ</a></button>
								<h3>SSL INTERCEPT IERAKSTU SKAITS: <?php countRecords("SSL_intercept") ?> <button><a href='/Functionality/SaveToFile.php?category=SSL_intercept&page=main'>SAGLABĀT FAILĀ</a></button>
									<h3>WITHOUT SSL INTERCEPT IERAKSTU SKAITS: <?php countRecords("without_SSL_intercept") ?> <button><a href='/Functionality/SaveToFile.php?category=without_SSL_intercept&page=main'>SAGLABĀT FAILĀ</a></button>
										<h3>WITH AUTHENTICATION IERAKSTU SKAITS: <?php countRecords("with_authentication") ?> <button><a href='/Functionality/SaveToFile.php?category=with_authentication&page=main'>SAGLABĀT FAILĀ</a></button>
											<h3>WITHOUT AUTHENTICATION IERAKSTU SKAITS: <?php countRecords("without_authentication") ?> <button><a href='/Functionality/SaveToFile.php?category=without_authentication&page=main'>SAGLABĀT FAILĀ</a></button>
												<h3>DO NOT SCAN IERAKSTU SKAITS: <?php countRecords("Do_Not_Scan") ?><button><a href='/Functionality/SaveToFile.php?category=Do_Not_Scan&page=main'>SAGLABĀT FAILĀ</a></button>
													<h3>ADVERTISEMENT IERAKSTU SKAITS: <?php countRecords("Advertisement") ?><button><a href='/Functionality/SaveToFile.php?category=Advertisement&page=main'>SAGLABĀT FAILĀ</a></button>
                						<h3>KOPĒJAIS IERAKSTU SKAITS: <?php countAllRecords() ?><button><a href='/Functionality/saveAllFiles.php'>SAGLABĀT VISU</a></button></h3>
                <!-- Ierakstu meklēšanas aile -->
                <div class="SearchAndSave">
                  <!-- Sadaļa ar tabulu, kurā tiek attēlots katrs ieraksts -->
                  <!-- Ierakstu meklēšanas teksta lauks, kuram piesaistīts JS skripts -->
                  <input type="text" id="myInput" onkeyup="SearchRecord()" placeholder="Meklēt.." title="Type in a name">
                </div>
                <!-- Tabula, kura attēlo visu darbību vēsturi -->
                <table style="width:100%" id="myTable">
                  <tr>
                    <th width="3%">ID</th>
                    <th width="3%">LIETOTĀJS</th>
                    <th width="5%">IP</th>
                    <th width="20%">DARBĪBA</th>
                    <th width="5%">KATEGORIJA</th>
                    <th width="20%">DARBĪBAS IEMESLS</th>
                    <th width="10%">LAIKS | DATUMS</th>
                    <th width="5%">IERAKSTU SKAITS</th>
                  </tr>
                  <!-- PHP skripts, kurš iegūst visus ierakstus no SQL datubāzes -->
                  <?php retrieveHistory() ?>
                </table>
      </div>

    </main>
</body>

</html>
