<?php
session_start();
if(!isset($_SESSION['loggedin'])){
        header('Location:index.php');
        exit;
}
if($_SESSION['username']==$_GET['user']){

}

else if($_SESSION['role']!='Darbinieks'){
        header('Location:main.php');
	exit;
}


?>
<?php include './Functionality/ConnectToDB.php'; ?>
<?php include './Functionality/PrepareStatement.php'; ?>
<?php include './Functionality/EditRecord.php'; ?>
<?php include './Functionality/CountRecords.php'; ?>


<!DOCTYPE html>
<!-- Valodas direktīva -->
<html lang="lv" dir="ltr">

<head>
  <!--Responsivitātes parametrs-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!-- PreventResubmit, lai izvairītos no atkārtotas dublikātu nosūtīšanas uz datubāzi -->
  <script src="./JS/PreventResubmit.js"></script>
  <!-- Meklēšanas ailes skripts, lai tabulā meklētu ierakstus -->
  <script src="./JS/SearchRecord.js"></script>
  <!--Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  <!--CSS ceļš -->
  <link rel="stylesheet" href="/Style/style.css">
  <!--Lapas nosaukums-->
  <title>Husky</title>

</head>

<body>
  <!--Apvalks visais lapai, papildus darbina grid -->
  <div class="wrapper">
    <!-- Lapas nosaukums un pārējā būtiskā informācija -->
    <header>
      <!-- Lapas nosaukums, galvenais Headeris -->
      <h1>PROFILA LAPA</h1>
      <!--Kas autorizējies un iespēja izlogoties.  -->
      <div class="Header_Login">
        <p><?=$_SESSION['username'] ?></p>
	<a href="/Functionality/logout.php?user=<?=$_SESSION['username']?>">IZLOGOTIES</a>
      </div>
    </header>
    <!--Navigācijas sadaļa, lai "Staigātu" starp lapām -->
    <nav>
      <!--Lapas nosaukuma konteineris -->
      <div class="PageTitle">
        <h1>H U S K Y</h1>
        <!-- Līnija, kas sadala h1 un h3 -->
        <div class="DividerLine"></div>
        <h3>SATURA ADAPTĀCIJAS SASKARNE</h3>
      </div>
      <!-- URL'S -->
      <a href="main.php" style="color:#8994b6;">SĀKUMS</a>
      <a href="super_blacklist.php">SUPER-BLACKLIST</a>
      <a href="blacklist.php">BLACKLIST</a>
      <a href="whitelist.php">WHITELIST</a>
      <a href="super_whitelist.php">SUPER-WHITELIST</a>
      <a href="ssl_intercept.php">SSL INTERCEPT</a>
			<a href="without_ssl_intercept.php">WITHOUT SSL INTERCEPT</a>
			<a href="with_authentication.php">WITH AUTHENTICATION</a>
			<a href="without_authentication.php">WITHOUT AUTHENTICATION</a>
			<a href="do_not_scan.php">DO NOT SCAN</a>
			<a href="advertisement.php">ADVERTISEMENT</a>
    </nav>
    <main>
      <!-- Attēlo lietotāja informāciju tabulas veidā -->
      <div class="UserInformation">
        <table>
  					<tr>
  						<td width="50%">Lietotājvārds:</td>
  						<td><?php retrieveUserInformation("Username",$_GET['user'])?></td>
  					</tr>
            <tr>
  						<td width="50%">Vārds:</td>
  						<td><?php retrieveUserInformation("Name",$_GET['user'])?></td>
  					</tr>
            <tr>
  						<td width="50%">Uzvārds:</td>
  						<td><?php retrieveUserInformation("Surname",$_GET['user'])?></td>
  					</tr>
  					<tr>
  						<td width="50%">Epasts:</td>
  						<td><?php retrieveUserInformation("Email",$_GET['user'])?></td>
  					</tr>
  					<tr>
  						<td width="50%">Loma:</td>
  						<td><?php retrieveUserInformation("Role",$_GET['user'])?></td>
  					</tr>
            <tr>
  						<td width="50%">IP adrese:</td>
  						<td><?php retrieveUserInformation("IPAddress",$_GET['user'])?></td>
  					</tr>
            <tr>
  						<td width="50%">Pēdējo reizi redzēts:</td>
  						<td><?php retrieveUserInformation("LastTimeSeen",$_GET['user'])?></td>
  					</tr>
            <tr>
  						<td width="50%">Pēdējā darbība:</td>
  						<td><?php retrieveUserInformation("LastAction",$_GET['user'])?></td>
  					</tr>
            <tr>
              <td width="50%">Darbības iemesls:</td>
              <td><?php retrieveUserInformation("ActionReason",$_GET['user'])?></td>
            </tr>
  				</table>

      </div>
      <!-- Lietotāja darbību vēsture -->
      <form class="Record_List" method="post">
        <!-- Meklēšanas aile -->
      <div class="SearchAndSave">
          <!-- Sadaļa ar tabulu, kurā tiek attēlots katrs ieraksts -->
          <!-- Ierakstu meklēšanas teksta lauks, kuram piesaistīts JS skripts -->
          <input type="text" id="myInput" onkeyup="SearchRecord()" placeholder="Meklēt.." title="Type in a name">
        </div>
        <!-- Pati tabula ar darbību vēsturi -->
        <table style="width:100%" id="myTable">
          <tr>
            <th width="33.33%">PĒDĒJO REIZI REDZĒTS</th>
            <th width="33.33%">PĒDĒJĀ DARBĪBA</th>
            <th width="33.33%">PĒDĒJĀS DARBĪBAS IEMESLS</th>
          </tr>
          <!-- PHP skripts, kurš iegūst visus ierakstus no SQL datubāzes -->
          <?php retrieveUserHistory($_GET['user']) ?>
        </table>
      </form>

    </main>
</body>

</html>
