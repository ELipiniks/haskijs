<?php

/*
* CountRecords.php skaita katrā kategorijas SQL tabulā ierakstu skaitu
* un izdod paziņojumu par skaitu.
*/

//Saskaitīt vienā kategorijas tabulā ierakstu skaitu.
function countRecords($category)
{
  $db = new Db();
  //SELECT SQL vaicājums, kurš skaita ierakstu skaitu
  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM $category WHERE 'id'>=0");
  //is_array un is_object nodrošina, lai nemet erroru, par tukšu foreach, ja tabulā ierakstu neeksistē (vai pašas tabulas).
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      //Vienreiz izvada ierakstu skaitu
      echo $row['SKAITS'];
    }
  } else {
    //Ja nekā nav, izvada 0;
    echo 0;
  }
}

//Saskaita kopējo ierakstu skaitu visās tabulās
function countAllRecords()
{
  $db = new Db();
  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM whitelist WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $row['SKAITS'];
    }
  } else {
    $summa = 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM blacklist WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM super_blacklist WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM super_whitelist WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM SSL_intercept WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM without_SSL_intercept WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM with_authentication WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM without_authentication WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM Do_Not_Scan WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }

  $rows = $db->select("SELECT COUNT(ID) AS SKAITS FROM Advertisement WHERE 'id'>=0");
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      $summa = $summa + $row['SKAITS'];
    }
  } else {
    $summa = $summa + 0;
  }
  //Izvada kopējo summu
  echo $summa;
}
