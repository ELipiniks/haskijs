<?php
/*
  EditRecord.php būtība ir noteikt adreses klasi automātiski, kad tiek pievienots
  jauns ieraksts, attiecīgi, noteikt vai tas ir URL/Domēns/IP/Cits .
  Kā arī noņemt visas slīpsvītras adreses beigās, lai nejauktu "prātu" kodam
  tālāk, nosakot, kas tā ir par kategoriju URL/Domēns.
  Ja tiek ievadītas vairākas adreses iekš teksta lauka, tad tiek arī veikta
  pārbaude uz ierakstu atstarpēm un tās tiek noņemtas.
*/


//Funkcija nosaka vai padotā vērtība ir Domēns
function determineIfValueIsDomain($formValue)
{
  $domainRegexPattern="/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/";
  return preg_match($domainRegexPattern, $formValue);
}

// Funkcijas nosaka vai padotā vērtība ir URL
function determineIfValueIsURL($formValue)
{
  $urlRegexPattern='/((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/';
  return preg_match($urlRegexPattern, $formValue);
}

// Funkcija nosaka vai padotā vērtība ir IP adrese
function determineIfValueIsIP($formValue)
{
  $ipRegexPattern='/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/';
  return preg_match($ipRegexPattern, $formValue);
}

/*
  Funkcija apkopo augstāk definētās funkcijas un katrai no trīs
  funkcijām padod vērtību, beigās tiek noteikts pie kādas kategorijas
  pieder padotā vērtība, ja neatbilst nevienai no augstāk definētajām
  kategorijām, tad tas tiek definēts kā "Cits."
  Funkcija DetermineValue atgriež String tipa mainīgo ar tekstu
  kādu no vārdiem: Domēns,URL,IP,Cits.
*/
function determineValue($recordValue)
{
  if( determineIfValueIsDomain($recordValue) == True )
  {
    return "Domēns";
  }
  else if( determineIfValueIsURL($recordValue) == True )
  {
    return "URL";
  }
  else if( determineIfValueIsIP($recordValue) == True )
  {
    return "IP";
  }
  else {
    return "Cits";
  }
}

// Funkcija pārbauda vai vērtībai pašās beigās ir kāda slīpsvītra.
function isSlashAtTheEnd($formValue)
{
  if(substr($formValue , -1)=='/')
  {
    // Ja beigās ir slīpsvītra, tad atgriež 1
    return 1;
  }
  else
  {
    return 0;
  }
}

// Funkcija, kura noņem slīpsvītras pašās beigās
function removeLastSlash($recordToTrim)
{
  $recordToTrim=substr_replace($recordToTrim ,"",-1);
  return $recordToTrim;
}

/*
  Funkcija, kura attīra no tukšajiem masīva ierakstiem,
  saņem masīvu, kurš satur Address_Value ierakstus un ja ir
  atstarpe starp ierakstiem, tad tā noņem visus tukšumus un atstarpes.
*/
function removeEmptyArrayValues($arrayOfValues)
{
  // Tiek noņemti tukšumi
  $arrayOfValues = explode("\n", str_replace("\r", "", $arrayOfValues));
  // Saskaitām cik ir ierakstu
  $itemCount = count($arrayOfValues);
  $i=0;
  // Tiek definēts jauns,tīrs masīvs
  $nonEmptyRecords=array();
  for($num = 0; $num < $itemCount; $num += 1)
  {
    // Ja vērtība nav tukšums, tad katra rinda tiek ierakstīta iekš jaunā masīva
    if(!empty($arrayOfValues[$num]))
    {
      $nonEmptyRecords[$i]=$arrayOfValues[$num];
      $i++;
    }
  }
  // Jaunais masīvs tiek atgriezts atpakaļ.
  return $nonEmptyRecords;
}

/*
  Funkcija, noņem visus tukšos ierakstus un noņem slīpsvītras beigās(ja tādas eksistē)
*/

function cleanUpRecord($domainValue)
{
  $domainValue = trim($domainValue);
  $arrayOfRecords = removeEmptyArrayValues($domainValue);
  $countOfRecords = count($arrayOfRecords);
  for($iteration = 0; $iteration < $countOfRecords; $iteration++)
  {
    //Nosaka vai beigās ir slīpsvītra
    if(isSlashAtTheEnd($arrayOfRecords[$iteration])==1)
    {
      //Noņem slīpsvītru beigās ja tāda eksistē
      $arrayOfRecords[$iteration] = removeLastSlash($arrayOfRecords[$iteration]);
    }
  }
  // Tiek atgriezts masīvs bez slīpsvītrām beigās
  return $arrayOfRecords;
}





?>
