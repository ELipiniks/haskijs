<?php

/*
*  editRow.php saņem vērtības no edit.php (ID, ieraksts, labošanas iemesls un kategorija),
*  tad balstoties uz izvēlēto ID un ierakstu tiek izpildīts SQL update vaicājums, lai
*  nomainītu ieraksta vērtību, labojuma iemeslu, lietotāju, datumu un laiku.
*/

//Tiek sākta sesija.
session_start();
include 'ConnectToDB.php';
include 'EditRecord.php';
include 'userIPAddress.php';
$db = new Db();
$valueID = $_POST['Edit_id']; //Labojamā ieraksta id
$newValue = $_POST['New_Value']; //Labojamā ieraksta vērtība
$newReason = $_POST['New_Reason']; //Labošanas iemesls
$user = $_GET['user']; //Lietotāja lietotājvārds
$category= $_POST['Edit_category']; //Kategorija, kurā tiek veikts labojums
//Latvijas laika josla
date_default_timezone_set("Europe/Riga");
//Datums un laiks (gads,mēnesis,diena) un (stunda,minūte,sekunde)
$dateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
/*
  Kad ieraksts tiek labots, tad viņam mainās tā veids(url,domēns,cits), tādēļ
  ir jāveic ieraksta veida pārbaude atkārtoti.
*/
//determineValue (atsauce: EditRecord.php)
$updatedAdressCategory = determineValue($newValue);

//SQL quote īsti nepildīja savu uzdevumu, tādēļ manuāli ielieku iekavās
$newValue = "'".$newValue."'";
$valueID = "'".$valueID."'";
$dateAndTime = "'".$dateAndTime."'";
$newReason = "'".$newReason."'";
$updatedAdressCategory = "'".$updatedAdressCategory."'";

//Tiek iegūta lietotāja IP adrese (atsauce: UserIPAddress.php)
$ipAddress = getUserIpAddr();

//Ieraksta atjaunināšanas vaicājums
$queryStatus = $db -> query(
"UPDATE `whitelist` SET
-- -- jaunā vērtība
`Address` = $newValue,
-- -- -- labojuma datums un laiks
`UploadTime` = $dateAndTime,
-- -- -- labojuma iemesls
`UploadReason` = $newReason,
-- -- -- atjaunināts veids
`Category` = $updatedAdressCategory,
-- -- tiek nomainīts labošanas karogs - ieraksts tika labots
`EditStatus` = 'nav' WHERE `id`=$valueID");

//Izveidot jaunu ierakstu darbību vēsturē - tika labots ieraksts.
$db->PrepareHistoryStatement($user, $ipAddress, 'Ieraksta labošana', $category, $newReason, $dateAndTime, '1');
//Izveidot jaunu ierakstu lietotāja darbības vēsturē
$db->PrepareUserStatement(
$user, //Lietotājvārds
$_SESSION['name'], //Vārds
$_SESSION['surname'], //Uzvārds
$_SESSION['email'] , //epasts
$_SESSION['role'],
$ipAddress,
$dateAndTime,
'Ieraksta labošana',
$newReason);

//Doties atpakaļ uz kategoriju, kur tika veikts labojums
header("Location:/$category.php");

 ?>
