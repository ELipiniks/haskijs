<?php

/*
Skripts saglabā KONKRĒTU kategorijas veidu failā, nolasot no datubāzes
visus ierakstus un ierakstot to failā, ja fails neeksistē, tad tāds tiek
automātiski izveidots.
*/
session_start();
include 'ConnectToDB.php';
include 'EditRecord.php';
include 'RecreateStructure.php';
include 'userIPAddress.php';
      //Izveidots datubāzes objekts


      // //Tiek iegūts kategorijas nosaukums ar kuru tiks strādāts.
      $selectedCategory = $_GET['category'];
      //
      // /*
      // Tiek iegūta informācija no kādas lapas iegūts - galvenās vai atsevišķas kategorijas lapas.
      // main - galvenā lapa; sub - kategorijas lapa
      // */
      $pageType = $_GET['page'];
      // //Izgūstam lietotāju, kurš veic šo darbību
	    // $user = $_GET['user'];
      // Tiek izveidots savienojums ar datubāzi un izgūti visi kategorijas ieraksti

      // Pārbaude vai folderis BCList eksistē, ja neeksistē - izveidot
      isFolderExistant('BCList');
      // Pārbaude vai fails BCList.txt eksistē, ja neeksistē - izveidot
      isFileExistant('BCList','BCList.txt');
      //Pārbaudīt vai tāda kategorija ir jau failā
      if(RecordSetExists($selectedCategory)){
        //Ja ir, tad izdzēst to kategoriju no "Define" līdz pašam "end"
        eraseOldRecordSet($selectedCategory);
        //Appendot failu un ierakstīt tur atjaunināto kategoriju
        $count = writeNewRecordSet($selectedCategory);
      }
      else{
        //Ja kategorija neeksistēja iepriekš, tad appendot failu ar jauno kategoriju
        $count = writeNewRecordSet($selectedCategory);
      }


      //Tiek iestatīta latvijas laika zona
      date_default_timezone_set("Europe/Riga");
      //Tiek izgūts tekošais datums un laiks
      $dateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
      //Tiek izgūts lietotāja IP
      $ipAddress = getUserIpAddr();

      //Ierakstam darbību vēsturē jaunu ierakstu
      $db->PrepareHistoryStatement($user, $ipAddress, 'Failu saglabāšana', $selectedCategory, 'Nav', $dateAndTime, $count);

      //Ierakstam darbību lietotāja vēsturē
	    $db->PrepareUserStatement(
        $user, //Lietotājvārds
        $_SESSION['name'], //Vārds
        $_SESSION['surname'], //Uzvārds
        $_SESSION['email'] , //epasts
        $_SESSION['role'], //loma
        $ipAddress,
        $dateAndTime,
        'Failu saglabāšana',
        'Nav');

/*
      Ja fails tiek saglabāts iekš kategorijas lapas, tad no tās lapas
      tiek padots "Page" parametrs "sub", tad tiek veikts redirekts uz to lapu atpakaļ,
      bet ja saglabāšana tiek veikta iekš galvenās lapas "main", tad
      Redirektēšana tiek veikta uz "main.php" (galveno lapu)
      */

      if($pageType=="sub")
      {
        //Dodas uz kategorijas lapu
        header("Location:/$selectedCategory.php");
      }
      else
      {
        //Dodas uz galveno lapu
        header("Location:/main.php");
      }
