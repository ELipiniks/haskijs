<?php
include 'PrepareStatement.php';
include 'ConnectToDB.php';
include 'EditRecord.php';
include 'userIPAddress.php';

/*
* uploadFile nodrošina failu atvēršanu, lasīšanu un apstrādāšanu, tālāk pasniedzot
* izgūtos ierakstus uz AddFileRecord.
*/
function uploadFile($category,$user)
{
  $db = new Db();
  if ($_FILES) {
    //Pārbaudām vai fails ir izvēlēts.
    if ($_FILES['file']['name'] != "") {

      //Pārbaudām vai ir plain text
      if (isset($_FILES) && $_FILES['file']['type'] != 'text/plain') {
        echo "<span>Fails netika pieņemts ! Lūdzu augšupielādēt .txt failu</span>";
        exit();
      }

      //Saglabājam pagaidu faila nosaukumu
      $fileName = $_FILES['file']['tmp_name'];

      //Izmest kļūdas paziņojumu ja neizdevās failu atvērt
      $file = fopen($fileName, "r") or exit("Neizdevās atvērt failu!");
      $uploadReason = $_POST['Upload_Reason'];
      $kategorija = $_GET['category'];
      $skaits=0;
      //Iestatām dublikātu karogu uz false
      $duplicateFlag=false;
      //Iestatām dublikātu skaitu uz nulli
      $duplicateCount=0;
      // Lasa .txt failu rindu pa rindai
      while (!feof($file)) {

        $line = fgets($file);
        $line = str_replace(array("\r", "\n"), '', $line);
        //No prepareStatement.php izsaucam funkciju, kura pārbauda vai ieraksts jau neeksistē
        $existance = checkIfRecordExists($line, $kategorija);
        //Ja eksistē, tad uzliekam dublikāta karogu uz True un ignorējam to vērtību
        if ($existance){
          $duplicateFlag=true;
          $duplicateCount++;
          continue;
        }
        else{
          //Pretējā gadījumā pievienojam datubāzei ierakstu
          $skaits++;
          // Ap izgūto līniju tiek veidotas ieraksta iekavas
          addFileRecord($kategorija, $line, $uploadReason,$user);
        }

      }
      fclose($file);

      //Izvēlamies Latvijas laika joslu
      date_default_timezone_set("Europe/Riga");
      //Balstoties uz laika joslu izgūstam datumu un laiku
      $TodaysDate =  date("Y/m/d") . " | " . date("H:i:s");
      //iegūt lietotāja ip adresi
      $IpAddress = getUserIpAddr();
      // Sagatavot vēstures ierakstu
      $db->PrepareHistoryStatement($user, $IpAddress, 'Ierakstu augšupielāde', $kategorija, $uploadReason, $TodaysDate, $skaits);
      //Sagatavot lietotāja vēstures ierakstu
      $db->PrepareUserStatement(
        $_SESSION['username'], //Lietotājvārds
        $_SESSION['name'], //Vārds
        $_SESSION['surname'], //Uzvārds
        $_SESSION['email'] , //epasts
        $_SESSION['role'], //loma
        $IpAddress, //ip adrese
        $TodaysDate, //datums
        'Ierakstu augšupielade',
        $uploadReason);
        // Ja ir dublikātu karogs, tad izsaukt JavaScript paziņojumu.
        if ($duplicateFlag)
        {
          echo '<script type="text/javascript">';
          echo 'alert("Tika sastapti '.$duplicateCount.' dublikāta ieraksti. Nekas nav jāveic - automātiski ignorēti");';
          echo 'window.location.href = "/'.$kategorija.'.php";';
          echo '</script>';
          // header("Location:/$kategorija.php");
          // phpAlert("Tika sastapti dublikāta ieraksti. Nekas nav jāveic - automātiski ignorēti");
        }
        else{
          //Ja nav dublikātu, tad var mierīgi doties atpakaļ uz kategorijas lapu
          header("Location:/$kategorija.php");
        }
    } else {
      if (isset($_FILES) && $_FILES['file']['type'] == '') {
        echo "<span>Izvēlies failu uzspiežot 'Choose File' un ierakstot pievienošanas iemeslu!</span>";
      }
    }
  }
}
?>
