<?php
/*
*  Authentication.php veic lietotāja autentifikāciju pret LDAP
*  serveri un izgūst būtisko informāciju par lietotāju - vārds, uzvārds,
*  epasts, grupa(s).
*/


//Var atkomentēt atkļūdošanas nolūkos, lai Authentication.php lapā attēlotu kļūdas
#error_reporting(E_ALL);
#ini_set('error_reporting', E_ALL);
#ini_set('display_errors', 'on');


//Tiek uzsākta sesija
session_start();

include 'userIPAddress.php';
include 'ConnectToDB.php';
include 'userController.php';

$db = new Db();

//Ja netiek iegūts lietotājvārds UN parole, tad izmest kļūdu
if (!isset($_POST['username']) && isset($_POST['password'])) {
  exit('Kļūda ievadot autentifikācijas datus!');
}

$ldaprdn = $_POST['username']; //Lietotājvārds
$ldappass = $_POST['password']; //Parole

$dn = "OU=proxy, DC=test-squid, DC=lv"; //Ceļš uz lietotāju Organizācijas Vienību (OU)

$config = parse_ini_file('config.ini');

/*
    Pirmais nepieciešamais solis ir pārbaudīt vai lietotājs ir lokālais Administrators,
    ja ir, tad viņam tiek iedota pilnīgas tiesības. Ja tas nav lokālais, tad
    notiek pārslēgšanās uz LDAP autentifikāciju
*/

//Pārbaudām vai lietotājvārds ir lokālais administrators
if (strcmp(trim($ldaprdn), $config['localAdmin']) == 0) {
  //Ja tas ir lokālais administrators, tad pārbaudām vai parole pareiza
  if (strcmp(trim($ldappass), $config['localPassword']) == 0) {
    //Tiek ģenerēts sesijas ID
    session_regenerate_id();
    $_SESSION['loggedin'] = TRUE; //Autentifikācijas karogs - Lietotājs ir veiksmīgi autentificēts
    $_SESSION['username'] = $ldaprdn; //Lietotājvārds
    $_SESSION['name'] = "Lokalais"; //Vārds
    $_SESSION['surname'] = "Administrators"; //Uzvārds
    $_SESSION['email'] = "admins@latvenergo.lv"; //epasts
    //Iedalām Administratora lomu
    $_SESSION['role'] = "Vadiba"; //loma

    //Rīgas laika josla
    date_default_timezone_set("Europe/Riga");
    //Datums (gads,mēnesis,diena) un laiks (stunda,minūte,sekunde)
    $DateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
    //Izgūt lietotāja IP adresi (atsauce: userIPAddress.php)
    $IpAddress = getUserIpAddr();
    
    //Nosūtīt uz galveno lapu
    header('Location: /main.php');
  } else {
    echo 'Nepareizs lietotājvārds vai parole!';
  }
  //Pretējā gadījumā pārbaudam vai ir LDAP lietotājs
} else {


  //Pieslēgšanās LDAP serverim "ldap://xxx.xxx.xxx.xxx", ja neizdodas, tad izdot paziņojumu
  $ldapconn = ldap_connect("ldap://192.168.87.15") or die("Neizdevās pieslēgties LDAP serverim!");

  //Tiek lietots konkrēti LDAPv3, lai klientu bibliotēkas pēc noklusējuma neizmanto LDAPv2
  ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

  /*
  Referālis (Refferal) ir indikācija, ka serveris nespēj turpināt ar pieprasījumu, bet ja
  tiks nosūtīts tas pats pieprasījums atkal uz norādīto serveri, tad tam jāturpina izpildīt pieprasījums.
  Klientu bibliotēkas var "dzīties" pēc refferāļiem (refferal) automātiski vai nē,
  bet, kad tie to dara, tad viņi izmantos atkārtoti tos pašus autentifikācijas
  datus no sākotnējā pieprasījumā. "Dzīšanās" pēc referāļiem var
  rezultēt drošības problēmās, tādēļ šo opciju atslēdzu..
*/
  ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

  /*
  Ja veiksmīgi ir izveidots savienojums ar LDAP serveri, tad tiek veikta Lietotāja
  autentificēšana pret serveri, datu validācija un izgūšana.
*/

  if ($ldapconn) {
    //ldap_bind autentificē lietotāju serverī.
    $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass) or die("Autentifikācijas kļūda!");
    if ($ldapbind) {
      //Masīvā tiek ievietoti:    vārds, uzvārds, epasts, grupa(s) - LDAP atribūti.
      $ldapAttributes = array("givenname", "sn", "mail", "memberof");
      /*
      ar ldap_search tiek meklēta lietotāja informācija, norādot serveri,
      ceļu uz lietotājiem (dn), lietotājs, kurš tiek meklēts un ldap informācijas atribūti.
    */
      $searchResult = ldap_search($ldapconn, $dn, "(cn=*$ldaprdn*)", $ldapAttributes) or die("Error" . ldap_error($ldapconn));

      /*
      ldap_get_entries tiek lietots, lai masīvu veidā var izgūt informāciju.
      Informāciju var izgūt sekojošos veidos:
      return_value["count"] = izgūto atribūtu skaits
      return_value[0] = pirmais atributs
      return_value[n] = n'tais atribūts
      return_value["attribute"]["count"] = konkrēta atribūta skaits
      return_value["attribute"][0] = pirmais konkrētais atribūts
      return_value["attribute"][i] = (i+1) konkrētais atribūts
    */

      $data = ldap_get_entries($ldapconn, $searchResult);

      //Tiek izgūts grupu skaits, kurām pieder lietotājs
      $groupCount =  $data[0]["memberof"]["count"];


      /*
      Balstoties pēc lietotāja izgūto grupu skaitu (cik grupām pieder) tiek izgūts
      katras grupas string sekojošā formātā: 'CN=grupa, OU=organizācija, DC=domēns, DC=lv'
      Ar explode $data[0]["memberof"][$i] string rinda tiek ievietota masīvā,
      dalot pēc komatiem. Un beigās tiek izvēlēta masīva 1. vērtība un noņems 'CN='
      un ierakstīts gala masīvā.
    */
      for ($i = 0; $i < $groupCount; $i++) {
        //Teksta rinda tiek dalīta pēc komatiem un ievietota masīvā
        $tempArray = explode(',', $data[0]["memberof"][$i]);
        //Tiek izvēlēts masīva 1. elements un noņemts 'CN='
        $groupName = str_replace('CN=', '', $tempArray[0]);
        // Gatavais grupas nosaukuma elements tiek ierakstīts grupu masīvā
        $groupArray[$i] = $groupName;
      }

      $adUserGroup = "Darbinieki"; //AD lietotāju grupas nosaukums
      $userFlag = false; //pēc noklusējuma nav lietotājs
      $adAdminGroup = "Vadiba"; //AD administratoru grupas nosaukums
      $adminFlag = false; //pēc noklusējuma nav lietotājs

      /*
      Lietotājs var atrasties vairākās grupās, tādēļ tiek iterēts caur
      izgūto grupu masīvu un meklēta piederība kādai no augstāk definētajām grupām
      Ja kāda piederība tiek atrasta, tad tiek nomainīts grupas karogs (userFlag/adminFlag)
    */
      foreach ($groupArray as $group) {
        if ($group == $adAdminGroup) {
          $adminFlag = true;
        } else if ($group == $adUserGroup) {
          $userFlag = true;
        }
      }

      /*
      Tiek pieņemta arī varbūtība, ka nejaušības dēļ lietotājs ir Administratora
      grupā vai lietotāja grupā, lai izvairītos konfliktus, tad vienmēr tiek izvēlēta
      lietotāja administratora grupa.
    */

      //Ja lietotājs ir administrators un lietotājs
      if ($adminFlag and $userFlag) {
        $role = 'Vadiba'; //tiek iestatīta administratora Loma
        //Ja lietotājs ir Administrators, bet nav lietotājs
      } else if ($adminFlag and !$userFlag) {
        $role = 'Vadiba'; //tiek iestatīta administratora loma
        //Ja nav administrators, bet ir lietotājs
      } else if (!$adminFlag and $userFlag) {
        $role = 'Darbinieks'; //tiek iestatīta lietotāja loma
        //Ja nekam nepieder
      } else {
        $role = 'Nav'; //Nav lomas un pieeja lietotnei tiks liegta
      }



      //Katru reizi lietotājam tiek ģenerēts jauns sesijas ID
      session_regenerate_id();

      //Saglabājam informāciju sesijas superglobālī
      $_SESSION['loggedin'] = TRUE; //Autentifikācijas karogs - Lietotājs ir veiksmīgi autentificēts
      $_SESSION['username'] = $_POST['username']; //Lietotājvārds
      $_SESSION['name'] = $data[0]["givenname"][0]; //Vārds
      $_SESSION['surname'] = $data[0]["sn"][0]; //Uzvārds
      $_SESSION['email'] = $data[0]["mail"][0]; //epasts
      $_SESSION['role'] = $role; //loma

      //Izveidot lietotāja tabulu iekš "myDB" datubāzes (CREATE TABLE IF NOT EXISTS)
      $isCreationSuccessful = $db->createUser($_SESSION['username']);
      //Rīgas laika josla
      date_default_timezone_set("Europe/Riga");
      //Datums (gads,mēnesis,diena) un laiks (stunda,minūte,sekunde)
      $DateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
      //Izgūt lietotāja IP adresi (atsauce: userIPAddress.php)
      $IpAddress = getUserIpAddr();

      //Pārvaude vai lietotājs ir pirmo reizi sistēmā (atsauce: userController.php)
      $UserIsNew = checkIfUserIsNew($_SESSION['username']);
      //Lietotājs ir pirmo reizi sistēmā
      if ($UserIsNew == 1) {
        //Tiek veikts ieraksts lietotāja darbības vēsturē.
        $db->PrepareUserStatement(
          $_SESSION['username'], //Lietotājvārds
          $_SESSION['name'], //Vārds
          $_SESSION['surname'], //Uzvārds
          $_SESSION['email'], //epasts
          $_SESSION['role'], //Loma
          $IpAddress, //Ip adrese
          $DateAndTime, //Pēdējo reizi redzēts
          'Pirmā reģistrācija', //Jaunākā
          'Nav' //Nav Darbības iemesla
        );
      } else {
        //Tiek veikts ieraksts lietotāja darbības vēsturē.
        $db->PrepareUserStatement(
          $_SESSION['username'], //Lietotājvārds
          $_SESSION['name'], //Vārds
          $_SESSION['surname'], //Uzvārds
          $_SESSION['email'], //epasts
          $_SESSION['role'], //loma
          $IpAddress, //Ip adrese
          $DateAndTime, //Pēdējo reizi redzēts
          'Autentifikācija', //Jaunākā darbība
          'Nav' //Nav darbībai konkrēta iemesla
        );
      }
      //Pārslēgties uz galveno lapu
      header('Location: /main.php');
    } else {
      echo "Nepareiza Parole vai/un lietotājvārds!";
    }
  }
}
