<?php
session_start();
include 'ConnectToDB.php';
include 'EditRecord.php';
include 'RecreateStructure.php';
include 'userIPAddress.php';
      //Izveidots datubāzes objekts
      $db = new Db();
      //Visas iespējamās datubāzes kategorijas
      $tables = array("whitelist","blacklist","super_whitelist","super_blacklist","SSL_intercept","without_SSL_intercept","with_authentication","without_authentication","Do_Not_Scan","Advertisement");
      //Iziterējam cauri katrai kategorijai
        $count=0;
        // Pārbaude vai folderis BCList eksistē, ja neeksistē - izveidot
        isFolderExistant('BCList');
        // Pārbaude vai fails BCList.txt eksistē, ja neeksistē - izveidot
        isFileExistant('BCList','BCList.txt');
      for($i=0;$i<10;$i++){
        //Pārbaudīt vai tāda kategorija ir jau failā
        if(RecordSetExists($tables[$i])){
          //Ja ir, tad izdzēst to kategoriju no "Define" līdz pašam "end"
          eraseOldRecordSet($tables[$i]);
          //Appendot failu un ierakstīt tur atjaunināto kategoriju
          $count = writeNewRecordSet($tables[$i]);
        }
        else{
          //Ja kategorija neeksistēja iepriekš, tad appendot failu ar jauno kategoriju
          $count = writeNewRecordSet($tables[$i]);
        }


        //Tiek iestatīta latvijas laika zona
        date_default_timezone_set("Europe/Riga");
        //Tiek izgūts tekošais datums un laiks
        $dateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
        //Tiek izgūts lietotāja IP
        $ipAddress = getUserIpAddr();

        $db->PrepareHistoryStatement($_SESSION['username'], $ipAddress, 'Failu saglabāšana', $tables[$i], 'Nav', $dateAndTime, $count);
      }

      //Ierakstam darbību lietotāja vēsturē
      $db->PrepareUserStatement(
        $_SESSION['username'],//Lietotājvārds
        $_SESSION['name'], //Vārds
        $_SESSION['surname'], //Uzvārds
        $_SESSION['email'] , //epasts
        $_SESSION['role'], //loma
        $ipAddress,
        $dateAndTime,
        'Visu ierakstu saglabāšana',
        'Nav');

      header("Location:/main.php");
