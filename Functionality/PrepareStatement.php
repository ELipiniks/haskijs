<?php
include 'userIPAddress.php';
/*
*  PrepareStatement.php ir kodols visam interfeisam, apkopojot vias funkcijas vienuviet,
*  kā arī ierakstot vērtības datubāzē un izvadot uz ekrāna.
*/


/*
*addFormRecord nodrošina manuālu raksta pievienošanu iekš kategoriju lapām, apstrādājot
*ierakstu ar vienu un vairākām rindām.
*/
function addFormRecord($selectedTable, $user)
{
  /*
    Tiek veikta pārbaude vai ieraksta vērtība un iemesls ir veiksmīgi
    padoti uz funkciju.
  */
  if (isset($_POST['Address_Value']) and isset($_POST['Submit_Reason'])) {

  $db = new Db();
  /*
    "Address_Value" ievades laukā var ievadīt vairākas rindas ar ierakstiem,
    tādēļ, lai pārūpētos, lai nebūtu tukšo lauku rindās tiek izmantots trim()
  */
  $_POST['Address_Value'] = trim($_POST['Address_Value']);
  $addressValue = $_POST['Address_Value']; //nesakārtots ierakstu strings
  $submitReason = $_POST['Submit_Reason']; //Pievienošanas iemesls
  // Tiek veikta ierakst attīrīšana no tukšiem ierakstiem un atgriež tīru masīvu
  /*
    Ieraksts tiek atkārtoti tīrīts un šoreiz starp ierakstu rindām
    tiek noņemtas atstarpes un slīpsvītras ierakstu beigās, lai
    veida noteikšanas funkcija var atšķirt domēnu no URL (atsauce:EditRecord.php)
  */
  $arrayOfAddresses = cleanUpRecord($addressValue); //Tiek izveidots ierakstu masīvs
  //Ja neeksistē, tad tiek izveidota kategorijas tabula
  $Notikums = $db->createRecordTable($selectedTable);
  //Ja neeksistē, tad tiek izveidota darbību vēstures tabula
  $Notikums = $db->createHistoryTable('RecordHistory');
  //Pievienošanas iemesls sagatavots PrepareStatement
  $submitReason = $db->quote($submitReason);
  //Skaitīt ierakstu rindiņu skaitu.
  $countOfRecords = count($arrayOfAddresses);
  //Dublikātu karogs pēc noklusējuma tiek iestatīts uz 'False'
  $duplicateFlag = False;
  //Dublikātu skaitītājs tiek iestatīts uz 0
  $duplicateCount = 0;
  //Tiek norādīta Latvijas laika josla
  date_default_timezone_set("Europe/Riga");
  //Izgūt šodienas datumu(gads,mēnesis,diena) un laiku (stunda,minūte,sekunde)
  $TodaysDate =  date("Y/m/d") . " | " . date("H:i:s");
  //Iegūt lietotāja IP adresi (userIPAddress.php)
  $IpAddress = getUserIpAddr();
  //Iterēt katrai ieraksta rindai $arrayOfAddresses masīvā
  for ($iteration = 0; $iteration < $countOfRecords; $iteration++) {
    // Pievienotās adreses tiek ievietotas iekavās
    $addressValue = $db->quote($arrayOfAddresses[$iteration]);
    //Pārbaudīt vai šāds ieraksts jau eksistē datubāzē (funkcija atrodama zemāk)
    $duplicate = checkIfRecordExists($addressValue, $selectedTable);
    //Ja dublicate == true
    if ($duplicate) {
      //dublikātu karogs tiek istatīts uz 'True'
      $duplicateFlag = True;
      //palielināt dublikātu skaitu
      $duplicateCount++;
      //Turpināt uz nākamo iterāciju, ignorējot dublikāta ierakstu
      continue;
    }
    //Noteikt ieraksta veidu (domēns, url ,cits) (atsauce:EditRecord.php)
    $adressCategory = determineValue($arrayOfAddresses[$iteration]);
    //Ierakstīt vērtību kategorijas tabulā
    $db->PrepareRecordStatement($selectedTable, $addressValue, $submitReason, $adressCategory, 'Manuali', 'Nav', $TodaysDate, $user);
  }
  //Ja tika sastapti dublikāti, paziņot par dublikātu sastapšanu
  if ($duplicateFlag) {
    phpAlert("Tika sastapti" . $duplicateCount . " dublikāta ieraksti. Nekas nav jāveic - automātiski ignorēti");
  }
  //Ierakstīt darbību darbības vēstures tabulā
    $db->PrepareHistoryStatement($user, $IpAddress, 'Ierakstu pievienošana', $selectedTable, $submitReason, $TodaysDate, $countOfRecords);
  //Ierakstīt darbību lietotāja darbības vēsturē
  $db->PrepareUserStatement(
    $_SESSION['username'], //Lietotājvārds
    $_SESSION['name'], //Vārds
    $_SESSION['surname'], //Uzvārds
    $_SESSION['email'], //epasts
    $_SESSION['role'],
    $IpAddress, //ip adrese
    $TodaysDate, //datums
    'Ieraksta manuāla pievienošana', //darbības iemesls
    $submitReason //pievienošanas iemesls
  );
}
}


/*
*addFileRecord nodrošina failu automātisku pievienošanu, augšupielādējot
*failus iekš kategorijas un tie automātiski tiek pievienoti. un kategorizēti.
*Šo funkciju izsauc failā "uploadFile.php"
*/
function addFileRecord($selectedTable, $rows, $uploadReason,$user){

  $db = new Db();
  //Iztīrīt visus tukšumus no ierakstu rindām
  $rows = trim($rows);
  //Izveidot kategorijas tabulu ja tāda neeksistē
  $Notikums = $db->createRecordTable($selectedTable);
  $uploadReason = $db->quote($uploadReason);
  // //Saskaitīt cik ierakstu ir iekš ierakstu masīva
  // $countOfRecords = count($arrayOfAddresses);
  //Iestatīta Latvijas laika josla
  date_default_timezone_set("Europe/Riga");
  //Balstoties uz laika joslu izgūt esošo datumu(gads,mēnesis,diena) un laiku(stunda,minūte,sekunde)
  $TodaysDate =  date("Y/m/d") . " | " . date("H:i:s");
  //Sagatavot vērtību priekš prepared statement
  $addressValue = $db->quote($rows);
  //Noteikt vērtības veidu (domēns,url, cits)
  $adressCategory = determineValue($rows);
  //  Ar prepared statements palīdzību ievietojam visu iekš SQL datubāzes
  $db->PrepareRecordStatement($selectedTable, $addressValue, $uploadReason, $adressCategory, 'Fails', 'Nav', $TodaysDate, $user);
}

/*
* Funkcija, lai sniegtu personalizētus Javascript paziņojumus tajā pašā logā, bez redirektiem.
*/
function phpAlert($message)
{
  echo '<script type="text/javascript">alert("' . $message . '")</script>';
}


/*
* retrieveRecords attēlo ierakstus tabulas veidā iekš individuālas kategorijas
*/
//selectedTable ir kategorija ar kuru vēlamies darboties
function retrieveRecords($selectedTable)
{
  $db = new Db();
  $rows = $db->select("SELECT `id`, `Address`,`UploadReason`,`Category`, `UploadMethod`, `EditStatus`, `UploadTime`, `Author`  FROM $selectedTable ORDER BY `id` DESC");
  //is_array un is_object nepieciešams, lai nerādītu warning/error, kad tabula ir tukša vai neeksistē
  if (is_array($rows) || is_object($rows)) {
    //Izvadīt katru ierakstu atsevišķi.
    foreach ($rows as $row) {
      echo "<tr>" .
        "<td>" . $row['id'] . "</td>" .
        "<td>" . $row['Address'] . "</td>" .
        "<td>" . $row['UploadReason'] . "</td>" .
        "<td>" . $row['Category'] . "</td>
         <td>" . $row['UploadMethod'] . "</td>
         <td>" . $row['EditStatus'] . "</td>
         <td>" . $row['UploadTime'] . "</td>
         <td>" . $row['Author'] . "</td>
         <td><input name='checkbox[]' type='checkbox' value=" . $row['id'] . "></td>" .
        "<td><button><a href='/edit.php?id=" . $row['id'] . "&category=" . $selectedTable . "&text=" . $row['Address'] . "'>Labot</a></button></td>" .
        "</tr>";
    }
  } else {
    //Ja tabula tukša, izvadīt burtiski neko
    echo " ";
  }
}


/*
*Izvada visu darbības vēsturi iekš main.php tabulas
*/
function retrieveHistory()
{
  // Veidojam jaunu DB objektu
  $db = new Db();
  //Tabula, kurā glabāsies darbību vēsture (definēt izveidi iekš addFormRecord)
  $actionHistoryTable ="RecordHistory";
  $rows = $db->select("SELECT `Id`, `User`,`IP`,`Action`,`Category`,`ActionReason`,`TimeOfAction`,`DataCount` FROM $actionHistoryTable ORDER BY `Id` DESC");
//is_array un is_object nepieciešams, lai nerādītu warning/error, kad tabula ir tukša vai neeksistē
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      echo "<tr>
      <td>" . $row['Id'] . "</td>
      <td><a href='./profile.php?user=". $row['User'] . "'</a>".$row['User']."</td>
      <td>" . $row['IP'] . "</td>
      <td>" . $row['Action'] . "</td>
      <td>" . $row['Category'] . "</td>
      <td>" . $row['ActionReason'] ."</td>
      <td>" . $row['TimeOfAction'] . "</td>
      <td>" . $row['DataCount'] . "</td>
      </tr>";
    }
  } else {
    //Ja neeksistē tabula vai ierakstu, izvadīt burtiski neko
    echo " ";
  }
}

/*
* Izgūst konkrētu lietotāju un konkrētu viņa informāciju pēc pieprasījuma.
* Ievietojot to tabulā par lietotāju: vārds, uzvārds utt. No SQL tabulas tiek izgūts
* jaunākais ieraksts par lietotāju.
* Funkcija tiek izsaukta iekš profile.php
*/
function retrieveUserInformation($column, $user){
	$db = new Db();
	$columnInfo = $column;
	$column = "`".$column."`";
  //izgūst jaunāko ierakstu ar DESC LIMIT 1
  $rows = $db->select("SELECT $column FROM $user ORDER BY `Id` DESC LIMIT 1");
  //is_array un is_object nepieciešams, lai nerādītu warning/error, kad tabula ir tukša vai neeksistē
	if (is_array($rows) || is_object($rows)) {
	  foreach($rows as $row){
		  echo $row[$columnInfo];
	  }
  }
  else {
    echo " ";
  }
}

/*
*Tiek izgūta konkrēta lietotāja darbības vēsture iekš viņa profila lapas (profile.php)
*/
function retrieveUserHistory($user)
{

  $db = new Db();
  //Izgūt lietotāja darbības vēsturi no jaunākā uz vecāko
  $rows = $db->select("SELECT `LastTimeSeen`, `LastAction`,`ActionReason` FROM $user ORDER BY `ID` DESC");
    //is_array un is_object nepieciešams, lai nerādītu warning/error, kad tabula ir tukša vai neeksistē
  if (is_array($rows) || is_object($rows)) {
    foreach ($rows as $row) {
      echo "<tr>
      <td>" . $row['LastTimeSeen'] . "</td>
      <td>" . $row['LastAction'] . "</td>
      <td>" . $row['ActionReason'] . "</td>
      </tr>";
    }
  } else {
    echo " ";
  }
}

/*
* Funkcija pārbaude iekš addFormRecord un uploadFile.php vai pievienotais ieraksts neeksistē jau datubāzē
*/
function checkIfRecordExists($AddressValue, $selectedTable)
{
  $db = new Db();
  //Tiek veikta salīdzināšana vai iegūtā vērtība nesakrīt kaut kur ar esošo vērtību iekš datubāzes
  $rows = $db->select("SELECT `id`, `Address` FROM $selectedTable WHERE `Address`='$AddressValue'");
  //Ja ir rezultāts, tad atgriežam 1
  if ($rows) {
    return 1;
  }
  //Pretējā gadījumā 0
  return 0;
}


?>
