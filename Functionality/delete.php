<?php

/*
  delete.php saņem vērtības no delete.php (Ārpus /Functionality) un
  balstoties uz izvēlētajiem ierakstu ID tiek veikts SQL DELETE vaicājums.
*/
//Sākt sesiju
  session_start();
  include 'ConnectToDB.php';
  include 'userIPAddress.php';
  $db = new Db();
  $recordsToDelete = $_POST['Delete_Data']; //Dzēšamo ierakstu ID string
  $actionReason = $_POST['Delete_Reason']; //Dzēšanas iemesls
  $user = $_GET['user']; //Lietotāja vārds, kurš dzēš
  $category = $_POST['Category']; //Kategorija, kurā tiek veikta dzēšana
  //Rīgas laika josla
  date_default_timezone_set("Europe/Riga");
  //Datums un laiks   (gads,mēnesis,diena)   (stunda,minūte,sekunde)
  $dateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
  //Lietotāja IP adrese (atsauce userIPAddress.php)
  $ipAddress = getUserIpAddr();
  //Ievietot iekš masīva ID, atdalot tos pēc komatiem.
  $idArray = explode(',', $recordsToDelete);
  //Saskaitīt cik ID vērtību ir iekš masīva
  $idCount = count($idArray);
  //Dzēšanas iemesla sagatavošana ievietošanai datubāzē
  $actionReason = $db->quote($actionReason);
  //Tiek iterēc cauri dzēšamo ierakstu ID masīvu.
  for ($iteration = 0; $iteration < $idCount; $iteration++) {
    // Pievienotās adreses tiek ievietotas iekavās
    $id = $db->quote($idArray[$iteration]);
    //Tiek dzēstis katrs ieraksts ar norādīto ID
    $result = $db -> query("DELETE FROM $category WHERE $category.`id` = $id");
  }
  //Saglabāt dzēšanas darbību darbību vēsturē
  $db->PrepareHistoryStatement($user, $ipAddress, 'Ierakstu dzēšana', $category, $actionReason, $dateAndTime, $idCount);
  //Saglabāt lietotāja vēsturē dzēšanas darbību
  $db->PrepareUserStatement(
        $user, //Lietotājvārds
        $_SESSION['name'], //Vārds
        $_SESSION['surname'], //Uzvārds
        $_SESSION['email'] , //epasts
        $_SESSION['role'], //loma
        $ipAddress, //ip adrese
        $dateAndTime, //datums un laiks
        'Ierakstu dzēšana', //Darbība
        $actionReason); //Darbības iemesls
  //Pēc dzēšanas atgriezties atpakaļ uz kategoriju
  header("Location:/$category.php");
 ?>
