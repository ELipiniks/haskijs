<?php
/*
*	Logout.php veic lietotāja sesijas iznīcināšanu, kā arī izlogošanās reģistrēšanu lietotājam
*/

//Atkļūdošanas nolūkos var atkomentēt 3 zemāk esošās rindas, lai redzētu kļūdu paziņojumus
#error_reporting(E_ALL);
#ini_set('error_reporting', E_ALL);
#ini_set('display_errors', 'on');

//Tiek sākta sesija
session_start();
include 'ConnectToDB.php';
include 'userIPAddress.php';
$db = new Db();

/*Šī rinda ir nepieciešama tīri, lai izveidotu savienojumu ar datubāzi, jo
 PreparedUserStatement nevēlas veidot savienojumu ar datubāzi.
 Šo ja kāds labo, varbūt var "atkost" un salabot
*/
$notikums=$db->createUser($_SESSION['username']);
//Tiek iestatīta Latvijas laika josla
date_default_timezone_set("Europe/Riga");
//Tiek izgūts datums un laiks (gads,mēnesis, diena) un (stunda,minūte,sekunde)
$dateAndTime =  date("Y/m/d") . " | " . date("H:i:s");
//Tiek izgūta lietotāja IP adrese (atsauce: userIPAddress.php)
$ipAddress = getUserIpAddr();

$db->PrepareUserStatement(
  $_SESSION['username'], //Lietotājvārds
  $_SESSION['name'], //Vārds
  $_SESSION['surname'], //Uzvārds
  $_SESSION['email'] , //epasts
  $_SESSION['role'], //loma
  $ipAddress,
  $dateAndTime,
  'Izrakstisanas',
  'Nav');


//Sesija tiek pārtraukta
session_destroy();
//Lietotājs tiek atgriezts uz autentifikācijas lapu
header('Location: /index.php');
?>
