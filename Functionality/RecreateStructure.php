<?php
/*
    Tiek veikta pārbaude vai folderis eksistē, ja neeksistē, tad tiek izveidots
    jauns. Jābūt izpildītam pirms faila eksistences validācijas.
    Tiek izmantots iekš SaveToFile.php
*/
function isFolderExistant($FolderName)
{
  $FolderExists=True;
  if ( !is_dir( "./$FolderName" ) )
  {

    $FolderExists=False;
	  mkdir( "./$FolderName" );

  }

  return $FolderExists;
}

/*
    Tiek veikta pārbaude vai fails eksistē, ja neeksistē, tad tiek izveidots
    jauns
*/
function isFileExistant($FolderName, $FileName)
{
  if ( !file_exists("./$FolderName/$FileName"))
  {
    $File=fopen("./$FolderName/$FileName", "w");
    fclose($File);
  }
}

/*
    Tiek atvērts fails un izdzēsti visi ieraksti, kuri sākas ar konkrētu
    "define category <kategorija>" un beidzas ar end.
*/
function eraseOldRecordSet($category){
  $out = array();
  $flag=0;
  //Tiek atvērts fails, kurā glabājas visa informācija
  foreach(file("./BCList/BCList.txt") as $line) {
   //Ja atrod vēlamo kategoriju, iestata flag uz 1 un izlaiž to rindu
   if(strcmp(trim($line), "define category ". $category)==0){
     $flag=1;
     continue;
   }
   //Kamēr flag nav 1, visus ierakstus secīgi ieraksta masīvā
   if($flag!=1){
      $out[] = $line;
   }
   //Ja flag ir 1 un sastop pirmo end, tad flag iestata uz 0 un izlaiž to end ieskaitot
    if(strcmp(trim($line), "end")==0 and $flag==1){
     $flag=0;
     continue;
   }
 }

 //Tiek atkal atvērts tas pats fails
 $myfile = fopen("./BCList/BCList.txt", "w") or die("Neizdevās atvērt failu!");
 /*
    No out masīva iekš faila tiek rakstīts masīvs $out, kurš vairāk nesatur
    ierakstus no konkrētas kategorijas
 */
 foreach ($out as $line) {
   fwrite($myfile, $line);
 }
 fclose($myfile);
}

/*
    Ieraksta failā konkrētas kategorijas ierakstus, sākot no faila beigām
*/
function writeNewRecordSet($category){
  $db = new Db();
  //Izgūt visus ierakstus no datubāzes iekš masīva
  $rows = $db -> select("SELECT `Address` FROM $category");
  $commulativeText = "";
  $count=0;
  //Katra rinda tiek secīgi ierakstīta komulatīvajā tekstā (string)
  if (is_array($rows) || is_object($rows)) {
  foreach($rows as $row)
  {
   //Ja tas ieraksts ir pie URL kategorijas, tad rakstīt URL komulatīvajā tekstā
      $commulativeText.= $row['Address']."\n";
      //Skaitām izgūto ierakstu skaitu
      $count++;
  }
  // Tiek appendots fails, kur pēdējā rindā tiks rakstīti jaunie ieraksti
  $file = fopen("./BCList/BCList.txt", "a+") or die("Nevarēja atvērt failu.");
  //Lai neraksta vienā rindā ar pēdējo end - iet uz nākamo rindu
  fwrite($file, "define category ". $category);
  fwrite($file,"\n");
  //Rakstīt visus ierakstus.
  fwrite($file, $commulativeText) or die("Neizdevās ierakstīt failā!");
  fwrite($file,"");
  fwrite($file,"end");
  fwrite($file,"\n");
  fclose($file);
  return $count;
}
else{
  $file = fopen("./BCList/BCList.txt", "a+") or die("Nevarēja atvērt failu.");
  //Lai neraksta vienā rindā ar pēdējo end - iet uz nākamo rindu
  fwrite($file, "");
  $count=0;
  return $count;

}
}

/*
    Pārbauda vai konkrētā kategorija eksistē failā
*/
function RecordSetExists($category){
  $flag=0;
  foreach(file("./BCList/BCList.txt") as $line) {
   //Iterē cauri visam failam un meklē konkrēto "define category <kategorija>"
  if(strcmp(trim($line), "define category ". $category)==0){
    $flag=1;
  }
}
  return $flag;
}
