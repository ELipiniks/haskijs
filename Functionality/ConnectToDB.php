<?php

/*
* ConnectToDB.php ir kontrolieris visām SQL darbībām:
*  Savienošanās ar datubāzi.
*  Datubāzes izveide.
*  Tabulu izveide
*  SQL vaicājumu izpilde
*  Prepared Statement realizācija
*/

class Db
{

  protected static $connection;

  //Connect funkcija veic autentifikāciju datubāzē
  public function connect()
  {
    //Tiek pārbaudīts vai izdosies pieslēgties datubāzei
    if(!isset(self::$connection))
    {
      //Autentifikācijas kredentiāļi atrodas atsevišķā config.ini failā
      $config = parse_ini_file('config.ini');
      //No config.ini tiek izgūts lietotājvārds un parole.
      self::$connection = new mysqli('localhost',$config['username'],$config['password']);
    }

    if(self::$connection === false)
    {
      return false;
    }
    return self::$connection;
  }

  //SQL vaicājuma realizēšanas funkcija.
  public function query($query)
  {
    //Autentifikācija datubāzē
    $connection = $this -> connect();
    // 'myDB' tiek izvēlēta par noklusējuma datbuāzi, 'myDB' var mainīt uz sev tīkamu nosaukumu
    $db_selected = $connection->select_db("myDB");
    // Pārbaude vai 'myDB' eksistē
    if (!$db_selected)
    {
        // Izveidot datubāzi
        $sql = "CREATE DATABASE myDB";
        if ($this->query($sql) === TRUE)
        {
            echo "Datubāze veiksmīgi izveidota";
        }
    }
    $result = $connection -> query($query);
    return $result;
  }

//Kategorijas tabulas izveidošana ja tāda neeksistē.
  public function createRecordTable($selectedTable)
  {
    $table = "CREATE TABLE IF NOT EXISTS $selectedTable (
    -- Ieraksta ID
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    -- URL/Domēns (veids)
    Address VARCHAR(300) NOT NULL,
    -- Pievienošanas iemesls
    UploadReason VARCHAR(300) NOT NULL,
    -- Kāds veids ir ieraksts - URL vai Domēns
    Category VARCHAR(50) NOT NULL,
    -- Kādā metodē pievienots - fails, manuāli.
    UploadMethod VARCHAR(30) NOT NULL,
    -- Vai ieraksts kādreiz tika labots?
    EditStatus VARCHAR(5) NOT NULL,
    -- Pievienšanas laiks un datums
    UploadTime VARCHAR(30) NOT NULL,
    -- Kurš pievienoja ierakstu
    Author VARCHAR(50))";
    //Tabula tiek ievietota iekš vaicājuma
    $result = $this -> query($table);
    if($result === false)
    {
      echo "Neizdevas izveidot tabulu ".$selectedTable;
    }
  }

//Tabula priekš darbību vēstures, kura ir redzama iekš galvenās sadaļas (main.php)
  public function createHistoryTable($selectedTable)
  {
    $table = "CREATE TABLE IF NOT EXISTS $selectedTable (
    -- Ieraksta ID
    Id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    -- Lietotāja vārds, kurš veicis labojumus
    User VARCHAR(30),
    -- Lietotāja IP adrese
    IP VARCHAR(30) NOT NULL,
    -- Lietotāja veiktā darbība - Pievienot(manuāli,fails), labot, dzēst, saglabāt
    Action VARCHAR(30) NOT NULL,
    -- Kategorija, kurā tika veikta darbība
    Category VARCHAR(30) NOT NULL,
    -- Darbības iemesls
    ActionReason VARCHAR(50) NOT NULL,
    -- Kad un cikos tika veikta darbība
    TimeOfAction VARCHAR(60) NOT NULL,
    -- Cik ierakstu tika pievienots
    DataCount VARCHAR(50))";
    $result = $this -> query($table);
    if($result === false)
    {
      echo "Neizdevas izveidot tabulu! ";
    }
  }

  //Lietotāja izveides vaicājums (tiek izveidota tabula ar lietotāja vārdu)
  public function createUser($user)
  {
    $table = "CREATE TABLE IF NOT EXISTS $user (
    -- Lietotāja unikālais ID
    ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    -- Lietotājvārds
    Username VARCHAR(30) NOT NULL,
    -- Vārds
    Name VARCHAR(30) NOT NULL,
    -- Uzvārds
    Surname VARCHAR(30) NOT NULL,
    -- Epasts
    Email VARCHAR(50) NOT NULL,
    -- Lietotāja loma/pieejas līmenis šajā interfeisā
    Role VARCHAR(50) NOT NULL,
    -- Lietotāja IP adrese
    IPAddress VARCHAR(60) NOT NULL,
    -- Kad pēdējo reizi redzēts tiešsaistē
    LastTimeSeen VARCHAR(60) NOT NULL,
    -- Pēdējā veiktā darbība intefeisā
    LastAction VARCHAR(60) NOT NULL,
    -- Pēdējās veiktās darbības iemesls
    ActionReason VARCHAR(50))";
    $result = $this -> query($table);
    if($result === false)
    {
      echo "Neizdevas izveidot lietotāja ".$user." tabulu! Nepieciešama koda pārbaude!";
    }
  }



//Ierakstu izgūšanas funkcija
  public function select($query)
  {
    $rows = array();
    $result = $this -> query($query);
    if($result === false)
    {
      return false;
    }
    while ($row = $result -> fetch_assoc())
    {
      $rows[] = $row;
    }
    return $rows;
  }

// Funckija, lai izmestu kkļūdu paziņojumus
  public function error()
  {
    $connection = $this -> connect();
    return $connection -> error;
  }

//Sagatavot vērtību ievietošanai iekš tabulas
  public function quote($value)
  {
    $connection = $this -> connect();
    return $connection -> real_escape_string($value);
  }

//Sagatavo vaicājumu (Prepared Statement) funkcija, lai ievietotu ierakstus iekš kategorijām
  public function PrepareRecordStatement($selectedTable, $address, $uploadReason, $category, $uploadMethod, $editStatus, $uploadTime, $author)
  {
    $connection = $this -> connect();
    $stmt = $connection->prepare("INSERT INTO $selectedTable (Address, UploadReason, Category, UploadMethod, EditStatus, UploadTime, Author) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssss", $Address, $UploadReason, $Category, $UploadMethod, $EditStatus, $UploadTime, $Author);
    $Address = $address;
    $UploadReason = $uploadReason;
    $Category = $category;
    $UploadMethod = $uploadMethod;
    $EditStatus = $editStatus;
    $UploadTime = $uploadTime;
    $Author = $author;
    $stmt->execute();
    $stmt->close();
  }

//Sagatavo vaicājumu (Prepared Statement) funkcija, lai ierakstītu darbību vēsturi galvenajā lapā (main.php)
  public function PrepareHistoryStatement($user, $ip, $action, $category, $actionReason, $timeOfAction, $dataCount)
  {
    $connection = $this -> connect();
    $stmt = $connection->prepare("INSERT INTO RecordHistory (User, IP, Action, Category, ActionReason, TimeOfAction, DataCount) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssss", $User, $IP, $Action, $Category, $ActionReason, $TimeOfAction, $DataCount);
    $User = $user;
    $IP = $ip;
    $Action = $action;
    $Category = $category;
    $ActionReason = $actionReason;
    $TimeOfAction = $timeOfAction;
    $DataCount = $dataCount;
    $stmt->execute();
    $stmt->close();
  }

//Sagatavo vaicājumu (Prepared Statement) funkcija par individuālu lietotāju, lai saglabāta vēsturi konkrēti par lietotāju.
  public function PrepareUserStatement($username, $name, $surname, $email, $role, $ipAddress, $lastTimeSeen, $lastAction, $actionReason)
  {
    $connection = $this -> connect();
    $stmt = $connection->prepare("INSERT INTO $username (Username, Name, Surname, Email, Role, IPAddress, LastTimeSeen, LastAction, ActionReason) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssssss", $Username, $Name, $Surname, $Email, $Role, $IPAddress, $LastTimeSeen, $LastAction, $ActionReason);
    $Username = $username;
    $Name = $name;
    $Surname = $surname;
    $Email = $email;
    $Role = $role;
    $IPAddress = $ipAddress;
    $LastTimeSeen = $lastTimeSeen;
    $LastAction = $lastAction;
    $ActionReason = $actionReason;
    $stmt->execute();
    $stmt->close();
  }
}
